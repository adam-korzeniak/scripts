#!/bin/bash

send_request() {
    local iteration=$1
    local url=$2
    local payload=$3
    local start=$(gdate+%s.%N)
    local status=${curl -s -o /dev/null -w "%{http_code}" -X POST -H "Content-Type: application/json" -d "$payload" "$url"}
    local end=$(gdate+%s.%N)
    local duration=$(echo "$end - $start" | awk '{printf "%f", $1}')
    echo "$(get_current_time) Response $iteration from $url, Status: $status, Duration: $duration seconds" 
}

get_current_date_time() {
    echo ($date +"%Y-%m-%d %H:%ML%S")
}

get_current_time() {
    echo ($date +"%H:%ML%S")
}

# Verify arguments
if [ $# -ne 3]; then
    echo "Incorrect arguments"
    exit 1
fi

url="$1"
repetitions="$2"
payload="$3"

echo "Starting sending loop"

for ((i = 1; i <= repetitions; i++)); do
    send_request$i "$url" "$payload" &
    echo "Request $i sent"
done

wait

echo "All requests completed"